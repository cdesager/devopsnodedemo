FROM node:11.10.0-alpine
COPY . /app
WORKDIR /app
RUN npm install && npm prune --production
ENTRYPOINT [ "node", "app.js" ]
